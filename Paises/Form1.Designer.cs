﻿namespace Paises
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.LabelNome = new System.Windows.Forms.Label();
            this.LabelC = new System.Windows.Forms.Label();
            this.LabelR = new System.Windows.Forms.Label();
            this.LabelP = new System.Windows.Forms.Label();
            this.LabelMoeda = new System.Windows.Forms.Label();
            this.LabelD = new System.Windows.Forms.Label();
            this.LabelBandeira = new System.Windows.Forms.Label();
            this.ComboBoxPais = new System.Windows.Forms.ComboBox();
            this.ListViewCurrency = new System.Windows.Forms.ListView();
            this.ListViewIP = new System.Windows.Forms.ListView();
            this.PictureBoxBandeira = new System.Windows.Forms.PictureBox();
            this.ProgressBarLoad = new System.Windows.Forms.ProgressBar();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.LabelCapital = new System.Windows.Forms.Label();
            this.LabelRegion = new System.Windows.Forms.Label();
            this.LabelPopulation = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.sPYAboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxBandeira)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LabelNome
            // 
            this.LabelNome.AutoSize = true;
            this.LabelNome.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNome.Location = new System.Drawing.Point(92, 235);
            this.LabelNome.Name = "LabelNome";
            this.LabelNome.Size = new System.Drawing.Size(54, 19);
            this.LabelNome.TabIndex = 0;
            this.LabelNome.Text = "Nome";
            // 
            // LabelC
            // 
            this.LabelC.AutoSize = true;
            this.LabelC.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelC.Location = new System.Drawing.Point(483, 235);
            this.LabelC.Name = "LabelC";
            this.LabelC.Size = new System.Drawing.Size(62, 19);
            this.LabelC.TabIndex = 1;
            this.LabelC.Text = "Capital";
            // 
            // LabelR
            // 
            this.LabelR.AutoSize = true;
            this.LabelR.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelR.Location = new System.Drawing.Point(92, 293);
            this.LabelR.Name = "LabelR";
            this.LabelR.Size = new System.Drawing.Size(63, 19);
            this.LabelR.TabIndex = 2;
            this.LabelR.Text = "Região";
            // 
            // LabelP
            // 
            this.LabelP.AutoSize = true;
            this.LabelP.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelP.Location = new System.Drawing.Point(483, 281);
            this.LabelP.Name = "LabelP";
            this.LabelP.Size = new System.Drawing.Size(91, 19);
            this.LabelP.TabIndex = 3;
            this.LabelP.Text = "População";
            // 
            // LabelMoeda
            // 
            this.LabelMoeda.AutoSize = true;
            this.LabelMoeda.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMoeda.Location = new System.Drawing.Point(92, 347);
            this.LabelMoeda.Name = "LabelMoeda";
            this.LabelMoeda.Size = new System.Drawing.Size(60, 19);
            this.LabelMoeda.TabIndex = 4;
            this.LabelMoeda.Text = "Moeda";
            // 
            // LabelD
            // 
            this.LabelD.AutoSize = true;
            this.LabelD.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelD.Location = new System.Drawing.Point(483, 347);
            this.LabelD.Name = "LabelD";
            this.LabelD.Size = new System.Drawing.Size(158, 19);
            this.LabelD.TabIndex = 5;
            this.LabelD.Text = "Dominio da Internet";
            // 
            // LabelBandeira
            // 
            this.LabelBandeira.AutoSize = true;
            this.LabelBandeira.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBandeira.Location = new System.Drawing.Point(362, 110);
            this.LabelBandeira.Name = "LabelBandeira";
            this.LabelBandeira.Size = new System.Drawing.Size(78, 19);
            this.LabelBandeira.TabIndex = 6;
            this.LabelBandeira.Text = "Bandeira";
            // 
            // ComboBoxPais
            // 
            this.ComboBoxPais.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBoxPais.FormattingEnabled = true;
            this.ComboBoxPais.Location = new System.Drawing.Point(164, 234);
            this.ComboBoxPais.Name = "ComboBoxPais";
            this.ComboBoxPais.Size = new System.Drawing.Size(268, 24);
            this.ComboBoxPais.TabIndex = 7;
            this.ComboBoxPais.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPais_SelectedIndexChanged);
            // 
            // ListViewCurrency
            // 
            this.ListViewCurrency.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListViewCurrency.Location = new System.Drawing.Point(162, 326);
            this.ListViewCurrency.Name = "ListViewCurrency";
            this.ListViewCurrency.Size = new System.Drawing.Size(270, 81);
            this.ListViewCurrency.TabIndex = 11;
            this.ListViewCurrency.UseCompatibleStateImageBehavior = false;
            // 
            // ListViewIP
            // 
            this.ListViewIP.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListViewIP.Location = new System.Drawing.Point(647, 326);
            this.ListViewIP.Name = "ListViewIP";
            this.ListViewIP.Size = new System.Drawing.Size(186, 81);
            this.ListViewIP.TabIndex = 12;
            this.ListViewIP.UseCompatibleStateImageBehavior = false;
            this.ListViewIP.View = System.Windows.Forms.View.Details;
            // 
            // PictureBoxBandeira
            // 
            this.PictureBoxBandeira.BackColor = System.Drawing.Color.Khaki;
            this.PictureBoxBandeira.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxBandeira.Image")));
            this.PictureBoxBandeira.InitialImage = null;
            this.PictureBoxBandeira.Location = new System.Drawing.Point(487, 48);
            this.PictureBoxBandeira.Name = "PictureBoxBandeira";
            this.PictureBoxBandeira.Size = new System.Drawing.Size(176, 171);
            this.PictureBoxBandeira.TabIndex = 13;
            this.PictureBoxBandeira.TabStop = false;
            // 
            // ProgressBarLoad
            // 
            this.ProgressBarLoad.Location = new System.Drawing.Point(162, 439);
            this.ProgressBarLoad.Name = "ProgressBarLoad";
            this.ProgressBarLoad.Size = new System.Drawing.Size(181, 23);
            this.ProgressBarLoad.TabIndex = 14;
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStatus.Location = new System.Drawing.Point(159, 491);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(58, 19);
            this.LabelStatus.TabIndex = 15;
            this.LabelStatus.Text = "Status";
            // 
            // LabelCapital
            // 
            this.LabelCapital.AutoSize = true;
            this.LabelCapital.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelCapital.Location = new System.Drawing.Point(615, 235);
            this.LabelCapital.Name = "LabelCapital";
            this.LabelCapital.Size = new System.Drawing.Size(13, 16);
            this.LabelCapital.TabIndex = 16;
            this.LabelCapital.Text = "*";
            // 
            // LabelRegion
            // 
            this.LabelRegion.AutoSize = true;
            this.LabelRegion.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelRegion.Location = new System.Drawing.Point(161, 297);
            this.LabelRegion.Name = "LabelRegion";
            this.LabelRegion.Size = new System.Drawing.Size(13, 16);
            this.LabelRegion.TabIndex = 17;
            this.LabelRegion.Text = "*";
            // 
            // LabelPopulation
            // 
            this.LabelPopulation.AutoSize = true;
            this.LabelPopulation.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPopulation.Location = new System.Drawing.Point(615, 281);
            this.LabelPopulation.Name = "LabelPopulation";
            this.LabelPopulation.Size = new System.Drawing.Size(13, 16);
            this.LabelPopulation.TabIndex = 18;
            this.LabelPopulation.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Nova Square", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(77, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 25);
            this.label1.TabIndex = 19;
            this.label1.Text = "Csharp";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Nova Square", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(73, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 43);
            this.label2.TabIndex = 20;
            this.label2.Text = "SPY";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sPYAboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(896, 24);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // sPYAboutToolStripMenuItem
            // 
            this.sPYAboutToolStripMenuItem.Name = "sPYAboutToolStripMenuItem";
            this.sPYAboutToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.sPYAboutToolStripMenuItem.Text = "SPY About";
            this.sPYAboutToolStripMenuItem.Click += new System.EventHandler(this.sPYAboutToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Khaki;
            this.ClientSize = new System.Drawing.Size(896, 531);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LabelPopulation);
            this.Controls.Add(this.LabelRegion);
            this.Controls.Add(this.LabelCapital);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.ProgressBarLoad);
            this.Controls.Add(this.PictureBoxBandeira);
            this.Controls.Add(this.ListViewIP);
            this.Controls.Add(this.ListViewCurrency);
            this.Controls.Add(this.ComboBoxPais);
            this.Controls.Add(this.LabelBandeira);
            this.Controls.Add(this.LabelD);
            this.Controls.Add(this.LabelMoeda);
            this.Controls.Add(this.LabelP);
            this.Controls.Add(this.LabelR);
            this.Controls.Add(this.LabelC);
            this.Controls.Add(this.LabelNome);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Paises";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxBandeira)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelNome;
        private System.Windows.Forms.Label LabelC;
        private System.Windows.Forms.Label LabelR;
        private System.Windows.Forms.Label LabelP;
        private System.Windows.Forms.Label LabelMoeda;
        private System.Windows.Forms.Label LabelD;
        private System.Windows.Forms.Label LabelBandeira;
        private System.Windows.Forms.ComboBox ComboBoxPais;
        private System.Windows.Forms.ListView ListViewCurrency;
        private System.Windows.Forms.ListView ListViewIP;
        private System.Windows.Forms.PictureBox PictureBoxBandeira;
        private System.Windows.Forms.ProgressBar ProgressBarLoad;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Label LabelCapital;
        private System.Windows.Forms.Label LabelRegion;
        private System.Windows.Forms.Label LabelPopulation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sPYAboutToolStripMenuItem;
    }
}

