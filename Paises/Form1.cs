﻿

namespace Paises
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Paises.Servicos;
    using System.Threading.Tasks;

    public partial class Form1 : Form
    {

        #region Atributos
        private NetworkService networkService;

        private ApiServices apiServices;

        private DataService dataService;

        private List<Pais> Paises;

        private DialogService dialogService;


        #endregion





        public Form1()
        {
            InitializeComponent();       
            
            networkService = new NetworkService();

            apiServices = new ApiServices();

            dataService = new DataService();

            dialogService = new DialogService();

            LoadPaises();

           
        }

        private async void LoadPaises()
        {
             bool load;
            //ProgressBarLoad.Value = 0;

            LabelStatus.Text = "A actualizar Paises.....";
            var connection = networkService.CheckConnection();



            if (!connection.IsSuccess)
            {


                LoadLocalPaises();
                load = false;
            }
            else

            {
                await LoadApiRates();
                load = true;
            }


            if (Paises.Count == 0)
            {
                LabelStatus.Text = "Nao ha ligacao a internet" + Environment.NewLine +
                    "e nao foram previamente carregados os paises" + Environment.NewLine +
                    "Tente mais tarde";

                return;
            }


            ComboBoxPais.DataSource = Paises;
            ComboBoxPais.DisplayMember = "name";

            ListViewIP.Columns.Add("Domain");

            //ProgressBarLoad.Value = 100;

            LabelStatus.Text = "Paises actualizados com Sucesso...";
            if (load)
            {
                LabelStatus.Text = string.Format("Paises carregados da internet em {0:F}", DateTime.Now);
            }
            else
            {
                LabelStatus.Text = string.Format("Paises carregados da base de dados");
            }

            ProgressBarLoad.Value = 100;
        }
        private void LoadLocalPaises()
        {

            Paises = dataService.GetData();
        }

        private async Task LoadApiRates()
        {

            ProgressBarLoad.Value = 0;

            var response = await apiServices.GetPaises("https://restcountries.eu", "/rest/v2/all");

            Paises = (List<Pais>)response.Result;
            dataService.DeleteBD();
            dataService.SaveData(Paises);
        }

        private void ComboBoxPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pais select = ComboBoxPais.SelectedItem as Pais;

            LabelCapital.Text = select.capital;
            LabelRegion.Text = select.region;
            LabelPopulation.Text = select.population.ToString();

            ListViewIP.Items.Clear();
            foreach (var domain in select.topLevelDomain)
            {
                ListViewItem item;//criamos um item do tipo listviewitem


                item = ListViewIP.Items.Add(domain.ToString());

            }

            ListViewCurrency.Items.Clear();
            foreach (var currencie in select.currencies)
            {
                ListViewItem item;//criamos um item do tipo listviewitem

                if (!string.IsNullOrEmpty(currencie.code))
                {
                    item = ListViewCurrency.Items.Add(currencie.code.ToString());
                }
                else
                {
                    item = ListViewCurrency.Items.Add("N/A");

                }
                if (!string.IsNullOrEmpty(currencie.name))
                {
                    item = ListViewCurrency.Items.Add(currencie.name.ToString());
                }
                else
                {
                    item = ListViewCurrency.Items.Add("N/A");
                }

                if (!string.IsNullOrEmpty(currencie.symbol))
                {
                    item = ListViewCurrency.Items.Add(currencie.symbol.ToString());
                }
                else
                {
                    item = ListViewCurrency.Items.Add("N/A");
                }
            }





            //for (int idx = 0; idx <= 2; idx++)//ajustar as coluna ao conteudo
            //{
            //    ListViewIP.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            //}


        }

        private void sPYAboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Antonio Almeida" + Environment.NewLine
                + "Versão 1.0.0" + Environment.NewLine
                + "10/12/2017");
        }
    }
}
