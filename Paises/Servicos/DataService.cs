﻿using Paises.Modelos;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Paises.Servicos
{
    class DataService
    {
        private SQLiteConnection connection;

        private SQLiteCommand command;

        private DialogService dialogService;

        public DataService()
        {
            dialogService = new DialogService();

            //tabela paises
            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }

            var path = @"Data\BDPaises.sqlite";

            try
            {
                connection = new SQLiteConnection("Data Source=" + path);
                connection.Open();




                string sqlcommand = "create table if not exists paises(idpais integer primary key, name varchar(250), capital varchar(250), region varchar(250), population integer)";

                command = new SQLiteCommand(sqlcommand, connection);

                command.ExecuteNonQuery();



                //tabela moedas


                sqlcommand = "create table if not exists moedas (idmoeda integer primary key autoincrement, code varchar(250), name varchar(250), symbol char(3), idpais integer, foreign key(idpais) references paises(idpais))";

                command = new SQLiteCommand(sqlcommand, connection);

                command.ExecuteNonQuery();

                sqlcommand =
    "create table if not exists domains(iddomain integer primary key autoincrement, domain varchar(5), idpais integer, foreign key (idpais) references paises (idpais))";

                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();




            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
            }


        }

        public void SaveData(List<Pais> Paises)
        {
            //gravar paises
            try
            {
                int idpais = 1;
                foreach (var paises in Paises)
                {
                    string sql =
                       string.Format("insert into paises (idpais,name, capital, region,population) values (@idpais,@name,@capital,@region,@population)");


                    command = new SQLiteCommand(sql, connection);
                    command.Parameters.AddWithValue("@idpais", idpais);
                    command.Parameters.AddWithValue("@name", paises.name);
                    command.Parameters.AddWithValue("@capital", paises.capital);
                    command.Parameters.AddWithValue("@region", paises.region);
                    command.Parameters.AddWithValue("@population", paises.population);


                    command.ExecuteNonQuery();

                    //gravar moedas
                    foreach (var moeda in paises.currencies)
                    {
                        sql =
                          string.Format("insert into moedas (code, name, symbol, idpais) values (@code,@name,@symbol,@idpais)");


                        command = new SQLiteCommand(sql, connection);
                        if (moeda.code == null)
                        {
                            command.Parameters.AddWithValue("@code", "N/A");
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@code", moeda.code);
                        }
                        if (moeda.name == null)
                        {
                            command.Parameters.AddWithValue("@name", "N/A");
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@name", moeda.name);
                        }
                        if (moeda.symbol == null)
                        {
                            command.Parameters.AddWithValue("@symbol", "N/A");
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@symbol", moeda.symbol);
                        }

                        command.Parameters.AddWithValue("@idpais", idpais);

                        command.ExecuteNonQuery();
                    }

                    foreach (var domain in paises.topLevelDomain)
                    {
                        sql = string.Format("insert into domains(domain, idpais) values (@domain, @idpais)");

                        command = new SQLiteCommand(sql, connection);
                        command.Parameters.AddWithValue("@domain", domain);
                        if (paises.topLevelDomain == null)
                        {
                            command.Parameters.AddWithValue("@domain", "N/A");
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@domain", domain);
                        }


                        command.Parameters.AddWithValue("@idpais", idpais);

                        command.ExecuteNonQuery();
                    }


                    idpais++;
                }
                connection.Close();
            }


            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
            }
        }


        internal void DeleteBD()
        {
            try
            {
                string sqlcommand = string.Format("delete from paises;delete from moedas; delete from domains; update sqlite_sequence set seq=0");
                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro.");
            }


        }



        public List<Pais> GetData()
        {
            List<Pais> Paises = new List<Pais>();

            try
            {
                string sql = "select idpais, name, capital, region, population from paises";

                command = new SQLiteCommand(sql, connection);

                //lê cada registo 
                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Pais p = new Pais();


                    p.name = (string)reader["name"];
                    p.capital = (string)reader["capital"];
                    p.region = (string)reader["region"];
                    p.population = Convert.ToInt32(reader["population"]);
                    p.currencies = LoadMoeda(Convert.ToInt32(reader["idpais"]));
                    p.topLevelDomain = LoadDomains(Convert.ToInt32(reader["idpais"]));
                    Paises.Add(p);


                }
                connection.Close();

                return Paises;
            }

            catch (SQLiteException e)
            {
                dialogService.ShowMessage("Erro", e.Message);
                return null;
            }
        }

        private List<string> LoadDomains(int idpais)
        {
            List<string> domains = new List<string>();

            try
            {
                string sqlcommand = "select domain from domains where idpais =" + idpais;
                command = new SQLiteCommand(sqlcommand, connection);


                SQLiteDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {

                    domains.Add(reader["domain"].ToString());

                }
                return domains;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro");

                return null;
            }



        }

        private List<Currency> LoadMoeda(int idpais)
        {
            List<Currency> Moedas = new List<Currency>();

            try
            {

                string sqlcommand = "select code, name, symbol from moedas where idpais =" + idpais;
                command = new SQLiteCommand(sqlcommand, connection);


                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Currency m = new Currency();

                    m.code = (string)reader["code"];
                    m.name = (string)reader["name"];
                    m.symbol = (string)reader["symbol"];

                    Moedas.Add(m);

                };

                return Moedas;
            }





            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro");

                return null;
            }





        }





    }
}
